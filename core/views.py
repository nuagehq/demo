# Create your views here.
import gevent
import gevent.socket
from gevent.queue import Queue
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from PIL import Image
from random import choice, random

def home(request):
    return render_to_response('home.html', RequestContext(request))

def bg_task():
    " You can define some background task. "
    for i in range(1,10):
        print "background task", i
        gevent.sleep(2)

def long_task():
    for i in range(1,10):
        print i
        gevent.sleep()

def sleep_random_time(i, response_queue):
    " Sleep some random time."
    
    t = random() * 5
    gevent.sleep(t)
    response_queue.put("Task %d slept for %2.2fs." % (i, t))

def stream(request):
    """ An streamed content for django 1.4 """
    
    def iterator():

        # Single greenlet.
        t = gevent.spawn(long_task)
        t.join()

        yield "sleeping for 3 seconds...<br/>"

        gevent.sleep(3)

        yield "done<br>"

        yield "getting some ips...<br/>"

        urls = ['www.google.com', 'www.example.com', 'www.python.org', 'projects.unbit.it']
        jobs = [gevent.spawn(gevent.socket.gethostbyname, url) for url in urls]
        gevent.joinall(jobs, timeout=2)

        for j in jobs:
            yield "ip = %s<br/>" % j.value

        # Define a queue to hold results.
        response_queue = Queue()

        # Spawn several tasks.
        tasks = [gevent.spawn(sleep_random_time, i, response_queue) for i in range(10)]
        for i in range(10):
            yield response_queue.get() + "<br>"

        gevent.spawn(bg_task)

        yield "done!<br>"

    # We need this to avoid caching in nginx which destroy the streming.
    response = HttpResponse(iterator())
    response['X-Accel-Buffering'] = 'no'
    
    return response


def image(request):
    " Create a random image."

    # Image size.
    size = (200, 200)
    step = (10, 10)
    ink = ("red", "blue", "green", "yellow")

    # Base image.
    image = Image.new("RGB", size)

    for x in range(0, size[0], step[0]):
        for y in range(0, size[1], step[1]):
            image.paste(Image.new("RGB", step, choice(ink)), (x, y))
           
    response = HttpResponse(mimetype="image/png")
    image.save(response, "PNG")
    
    return response
    
